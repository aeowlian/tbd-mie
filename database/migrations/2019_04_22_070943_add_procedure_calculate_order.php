<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProcedureCalculateOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared(DB::raw('
        DROP PROCEDURE IF EXISTS `calculateOrder`;
        CREATE PROCEDURE calculateOrder(IN `orderID` INT)
        BEGIN
        CREATE TEMPORARY TABLE IF NOT EXISTS  priceTable(
            name INT,
            price INT
          );
          INSERT INTO priceTable(name, price)
          SELECT `id`, `order_price` 
          FROM `order_items`
          WHERE `order_id`=`orderID`;
          
          SELECT SUM(price) from priceTable;
        END;           
    '));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
