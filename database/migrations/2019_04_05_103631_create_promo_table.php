<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('promos', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->string('nama', 50);
        //     $table->boolean('active')->default(false);
        //     $table->integer('max_purchase')->unsigned()->default(0);
        //     $table->boolean('is_deleted')->default(false);
        //     $table->dateTime('active_until');
        // });
        DB::statement(
            DB::raw("
            CREATE TABLE `promos` (
                `id` bigint(20) UNSIGNED NOT NULL,
                `nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
                `active` tinyint(1) NOT NULL DEFAULT '0',
                `max_purchase` int(10) UNSIGNED NOT NULL DEFAULT '0',
                `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
                `active_until` datetime NOT NULL
            );
            "
            )
        );
        DB::statement(DB::raw("     
            ALTER TABLE `promos`
            ADD PRIMARY KEY (`id`);
        "));
        DB::statement(DB::raw("
            ALTER TABLE `promos`
            MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
        "));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('promos');
        DB::statement(DB::raw('DROP TABLE promos'));
    }
}
