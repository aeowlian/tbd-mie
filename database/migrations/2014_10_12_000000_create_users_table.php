<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('users', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->string('name');
        //     $table->string('email')->unique();
        //     $table->timestamp('email_verified_at')->nullable();
        //     $table->string('password');
        //     $table->boolean('is_admin')->nullable(false)->default(false);
        //     $table->rememberToken();
        //     $table->timestamps();
        // });\
        DB::statement(DB::raw("
            CREATE TABLE `users` (
                `id` bigint(20) UNSIGNED NOT NULL,
                `name` varchar(255) NOT NULL,
                `email` varchar(255) NOT NULL,
                `password` varchar(255) NOT NULL,
                `phone` varchar(50) NOT NULL,
                `address` varchar(255) NOT NULL,
                `is_admin` tinyint(1) NOT NULL DEFAULT '0'
            );
            "
        ));
        DB::statement(DB::raw("            
            ALTER TABLE `users`
            ADD PRIMARY KEY (`id`),
            ADD UNIQUE KEY `users_email_unique` (`email`);"
        ));
        DB::statement(DB::raw("
            ALTER TABLE `users`
            MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
        "));




    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('users');
        DB::statement(DB::raw('DROP TABLE users'));
    }
}
