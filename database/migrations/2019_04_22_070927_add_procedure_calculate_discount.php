<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProcedureCalculateDiscount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared(DB::raw('
        DROP PROCEDURE IF EXISTS `calculateDiscount`;
        CREATE PROCEDURE calculateDiscount(IN `order_id` bigint(20))
                   BEGIN
                           declare totalDiscount float;
                           declare str varchar(255);
                           declare idTemp,priceTemp,toppingIdTemp,lastId,temp,counterTopping,lastPriceTemp integer;
                           declare finished INTEGER DEFAULT 0;
                           DECLARE OrderTopping CURSOR FOR 
                           select
                               OrderItem.id,
                               order_price,
                               topping_id
                           from
                               (                
                                   select *
                                   from order_items
                                   where order_items.order_id = order_id
                                   ) as OrderItem LEFT JOIN item_toppings ON OrderItem.id = item_toppings.order_item_id
                           order by OrderItem.id ASC;
                           DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = 1;
                           
                           CREATE temporary table promoDiscount
                           select itemIdpromo,topping_id,discount,promoId,jml
                           from (
                               select promos.id as promoID,promo_items.id as itemIdPromo,topping_id,discount
                               from promos join promo_items
                               on promo_items.promo_id=promos.id
                           ) as t1 join (
                               select promos.id,count(promo_items.id) as jml
                               from promos join promo_items
                               on promo_items.promo_id=promos.id
                               group by promos.id
                           ) as t2 on t1.promoID = t2.id;
                           CREATE TEMPORARY TABLE tempPromo (Id int,topping_id int, discount float,idPromo int,jml int); 
                           CREATE TEMPORARY TABLE result (OrderItemID int,discountPrice int); 
       
                           set lastId=0;
                           set @str="";
                           set counterTopping=0;
                           OPEN OrderTopping;
                           topping_loop: LOOP
                               IF finished= 0 THEN FETCH OrderTopping INTO idTemp,priceTemp,toppingIdTemp;
                               END IF;
                               IF finished=1 THEN 
                                   set idTemp = 0;
                               END IF;
                               IF lastId != idTemp THEN
                                       IF @str!="" THEN
                                           PREPARE stmt FROM @str;
                                           EXECUTE stmt;
                                           DEALLOCATE PREPARE stmt;
                                           select idPromo into temp
                                           from tempPromo
                                           group by idPromo
                                           having count(idPromo)=counterTopping;
                                           
                                           select SUM(discount) into totalDiscount
                                           from tempPromo
                                           where idPromo=temp;
                                           IF totalDiscount=0 THEN
                                               select SUM(discount) into totalDiscount 
                                               from tempPromo
                                               where jml=1;
                                           END IF;
                                           insert into result values(lastId,totalDiscount*lastPriceTemp);
                                           TRUNCATE tempPromo;
                                           set counterTopping=0;
                                       END IF;
                                   set @str="insert into tempPromo select * from promoDiscount where ";
                                   set @str=CONCAT(@str,"topping_id=",toppingIdTemp);
                                   set lastId=idTemp;
                                   ELSE
                                        set @str=CONCAT(@str," OR ","topping_id=",toppingIdTemp);
                                        
                               END IF;
                               set lastId=idTemp;
                               set lastPriceTemp=priceTemp;
                               set counterTopping=counterTopping+1;
                               IF finished = 1 THEN LEAVE topping_loop;
                               END IF;
                           end loop;
       
                           CLOSE OrderTopping;
                           
                           select * from result;
                           
                           DROP temporary table tempPromo;
                           DROP temporary table result;
                           DROP temporary table promoDiscount;
                           
                   END;
        '));

        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `updateDiscount`;
            CREATE PROCEDURE updateDiscount(IN `order_item_id` bigint(20),IN `discountin` int(11))
            BEGIN
            UPDATE
                `order_items`
            SET
                `discount`=`discountin`
            WHERE
                `id`=`order_item_id`;
            END

        '));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
