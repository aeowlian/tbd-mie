<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProceduresForTopping extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `createTopping`;
            CREATE PROCEDURE createTopping(IN `name_input` varchar(50),IN `realPrice_input` INT, IN `price_input` INT, IN `path_input` varchar(255))
            BEGIN
                INSERT INTO
                    `toppings` (`name`,`real_price`, `price`, `path`)
                VALUES
                    (`name_input`,`realPrice_input`, `price_input`, `path_input`);
            END;            
        '));

        DB::unprepared(DB::raw('
        DROP PROCEDURE IF EXISTS `updateTopping`;
        CREATE PROCEDURE updateTopping(IN idTopping INT, IIN `namein` varchar(50),IN `realPricein` INT, IN `pricein` INT, IN `pathin` varchar(255))
        BEGIN
            UPDATE
                `toppings`
            SET
                `name`=namein,
                `real_price`=realPriceIn,
                `price`=pricein,
                `path`=pathin
            WHERE
                id=`idTopping;
        END; 
    '));
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS createTopping;       
        '));
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS updateTopping;       
        '));
    }
}
