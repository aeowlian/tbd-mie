<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProcedureForStatusUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `updateOrder`;
            CREATE PROCEDURE updateOrder(IN `idOrder` INT, IN `status` INT)
            BEGIN
                UPDATE
                    `orders`
                SET
                    `status`=status
                WHERE
                    `id`=idOrder;

                SELECT
                    *
                FROM
                    `orders`
                WHERE
                    `id`=idOrder;
            END; 
        '));

        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `updateOrderItem`;
            CREATE PROCEDURE updateOrderItem(IN `idOrderItem` INT, IN `status` INT)
            BEGIN
                UPDATE
                    `order_items`
                SET
                    `status`=status
                WHERE
                    `id`=idOrderItem;

                SELECT
                    *
                FROM
                    `order_items`
                WHERE
                    `id`=idOrderItem;
            END; 
        '));

        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `updateOrderItemDiscount`;
            CREATE PROCEDURE updateOrderItemDiscount(IN `idOrderItem` INT, IN `discount` INT)
            BEGIN
                UPDATE
                    `order_items`
                SET
                    `discount`=discount
                WHERE
                    `id`=idOrderItem;

                SELECT
                    *
                FROM
                    `order_items`
                WHERE
                    `id`=idOrderItem;
            END; 
        '));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `updateOrder`;
        '));
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `updateOrderItem`;
        '));
    }
}
