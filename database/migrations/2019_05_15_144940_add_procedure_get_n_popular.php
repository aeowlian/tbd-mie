<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProcedureGetNPopular extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::unprepared(DB::raw('
        DROP PROCEDURE IF EXISTS `mostPopular`;
        CREATE PROCEDURE mostPopular(IN `n` INT)
        BEGIN
            declare lastId,lastIdTopping,idTemp,toppingIdtemp,isFirst integer;
            declare str varchar(255);
            declare finished INTEGER DEFAULT 0;
            declare temp INTEGER DEFAULT 0;
            
            declare popularCursor cursor for
            select item_toppings.order_item_id,topping_id
            from (
                select order_item_id,count(topping_id) as jml
                from item_toppings
                group by order_item_id
            )as temp join item_toppings
            on temp.order_item_id=item_toppings.order_item_id
            where jml=n;
            DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = 1;
            
            create temporary table result(idOrderItem int,combination varchar(255)); 
            
            set @str="";
            set lastId=0;
            set isFirst=1;
            OPEN popularCursor;
            supa_loop: LOOP

                IF finished=0 THEN 
                    FETCH popularCursor INTO idTemp,toppingIdTemp;
                END IF;
                IF finished=1 THEN
                    set idTemp=0;
                END IF;
                IF temp = 0 THEN
                    set temp=1;
                    set @str=concat(@str,lastIdTopping);
                ELSE
                    set @str=concat(@str,",",lastIdTopping);
                END IF;
                IF lastId!=idTemp THEN
                    
                    IF isFirst=0 THEN
                        insert into result values(lastId,@str);
                    END IF;
                    set temp=0;
                    set @str="";

                END IF;
                set lastId=idTemp;
                set lastIdTopping=toppingIdTemp;
                IF isFirst=1 THEN
                    set isFirst=0;
                END IF;
                IF finished = 1 THEN LEAVE supa_loop;
                END IF;
            end loop;
            CLOSE popularCursor;
            
            select combination, count(combination) as jml from result group by combination order by jml desc limit 1;
            
            drop temporary table result;
        END;           

        '));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
