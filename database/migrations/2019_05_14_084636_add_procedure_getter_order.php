<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProcedureGetterOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `indexOrder`;
            CREATE PROCEDURE indexOrder()
            BEGIN
                SELECT
                    *
                FROM
                    `orders`;
            END; 
        '));

        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `getOrder`;
            CREATE PROCEDURE getOrder(IN `order_id` bigint(20))
            BEGIN
                SELECT
                    *
                FROM
                    `orders`
                WHERE
                    `id` = order_id;
            END; 
        '));

        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `getOrderByStatus`;
            CREATE PROCEDURE getOrderByStatus(IN `status` bigint(20))
            BEGIN
                SELECT
                    *
                FROM
                    `orders`
                WHERE
                    `status` = status;
            END; 
        '));

        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `deleteOrder`;
            CREATE PROCEDURE deleteOrder(IN `order_id` bigint(20))
            BEGIN
                DELETE FROM
                    `orders`
                WHERE
                    `id` = order_id;
            END; 
        '));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `indexOrder`;
        '));
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `getOrder`;
        '));
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `deleteOrder`;
        '));
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `getOrderByStatus`;
        '));
    }
}
