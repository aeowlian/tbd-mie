<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('menus', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->string('name', 50);
        //     $table->integer('price');
        //     $table->string('path', 255);
        // });

        DB::statement(DB::raw('
            CREATE TABLE `menus` (
                `id` bigint(20) UNSIGNED NOT NULL,
                `name` varchar(50) NOT NULL,
                `real_price` int(11) NOT NULL,
                `price` int(11) NOT NULL,
                `path` varchar(255) NOT NULL
            );
        '));
        
        DB::statement(DB::raw('
            ALTER TABLE `menus`
            ADD PRIMARY KEY (`id`);
        '));
        
        DB::statement(DB::raw('
            ALTER TABLE `menus`
            MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
        '));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('menus');
        DB::statement(DB::raw('
            DROP TABLE `menus`;
        '));
    }
}
