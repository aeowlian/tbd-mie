<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('orders', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->dateTime('created_at');
        // });
        DB::statement(
            DB::raw('
                CREATE TABLE `orders` (
                    `id` bigint(20) UNSIGNED NOT NULL,
                    `created_at` datetime NOT NULL,
                    `status` int(11) DEFAULT 0
                );

                '
            )
        );
        DB::statement(DB::raw("
            ALTER TABLE `orders`
            ADD PRIMARY KEY (`id`);
        "));
        DB::statement(DB::raw("
            ALTER TABLE `orders`
            MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
        "));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('orders');
        DB::statement(DB::raw('DROP TABLE orders'));

    }
}
