<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProceduresForMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `createMenu`;
            CREATE PROCEDURE createMenu(IN `name_input` varchar(50),IN `realPrice_input` INT, IN `price_input` INT, IN `path_input` varchar(255))
            BEGIN
                INSERT INTO
                    `menus` (`name`,`real_price`, `price`, `path`)
                VALUES
                    (`name_input`,`realPrice_input`, `price_input`, `path_input`);
            END;           
        '));

        DB::unprepared(DB::raw('
        DROP PROCEDURE IF EXISTS `updateMenu`;
        CREATE PROCEDURE updateMenu(IN `idMenu` INT, IN `namein` varchar(50),IN `realPricein` INT, IN `pricein` INT, IN `pathin` varchar(255))
        BEGIN
            UPDATE
                `menus`
            SET
                `name`=namein,
                `real_price`=realPriceIn,
                `price`=pricein,
                `path`=pathin
            WHERE
                `id`=idMenu;
        END; 
        '));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS createMenu;       
        '));
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS updateMenu;       
        '));
    }
}
