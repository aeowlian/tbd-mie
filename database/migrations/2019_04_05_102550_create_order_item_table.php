<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('order_items', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->bigInteger('order_id')->unsigned();
        //     $table->bigInteger('menu_id')->unsigned();
        //     $table->integer('order_price');
        // });

        // Schema::table('order_items', function($table) {
        //     $table->foreign('order_id')->references('id')->on('orders');
        //     $table->foreign('menu_id')->references('id')->on('menus');
        // });

        DB::statement(DB::raw('
            CREATE TABLE `order_items` (
                `id` bigint(20) UNSIGNED NOT NULL,
                `order_id` bigint(20) UNSIGNED NOT NULL,
                `menu_id` bigint(20) UNSIGNED NOT NULL,
                `order_price` int(11) NOT NULL,
                `discount` int(11) NOT NULL DEFAULT 0,
                `status` int(11) DEFAULT 0
            );
        '));

        DB::statement(DB::raw('
            ALTER TABLE `order_items`
            ADD PRIMARY KEY (`id`),
            ADD KEY `order_items_order_id_foreign` (`order_id`),
            ADD KEY `order_items_menu_id_foreign` (`menu_id`);
        '));

        DB::statement(DB::raw('
            ALTER TABLE `order_items`
            ADD CONSTRAINT `order_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`),
            ADD CONSTRAINT `order_items_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`);
        '));
        
        DB::statement(DB::raw('
            ALTER TABLE `order_items`
            MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
        '));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('order_items');
        DB::statement(DB::raw('
            DROP TABLE `order_items`;
        '));
    }
}
