<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProcedureGetterOrderItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `indexOrderItem`;
            CREATE PROCEDURE indexOrderItem()
            BEGIN
                SELECT
                    *
                FROM
                    `order_items`;
            END; 
        '));

        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `getOrderItem`;
            CREATE PROCEDURE getOrderItem(IN `order_item_id` bigint(20))
            BEGIN
                SELECT
                    *
                FROM
                    `order_items`
                WHERE
                    `id` = order_item_id;
            END; 
        '));

        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `getOrderItemByOrder`;
            CREATE PROCEDURE getOrderItemByOrder(IN `order_id_in` bigint(20))
            BEGIN
                SELECT
                    *
                FROM
                    `order_items`
                WHERE
                    `order_id` = order_id_in;
            END; 
        '));

        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `deleteOrderItem`;
            CREATE PROCEDURE deleteOrderItem(IN `order_item_id` bigint(20))
            BEGIN
                DELETE FROM
                    `order_items`
                WHERE
                    `id` = order_item_id;
            END; 
        '));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `indexOrderItem`;
        '));
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `getOrderItemByOrder`;
        '));
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `getOrderItem`;
        '));
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `deleteOrderItem`;
        '));
    }
}
