<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemToppingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('item_toppings', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->bigInteger('order_item_id')->unsigned();
        //     $table->bigInteger('topping_id')->unsigned();

        //     $table->foreign('order_item_id')->references('id')->on('order_items');
        //     $table->foreign('topping_id')->references('id')->on('toppings');
        // });

        DB::statement(DB::raw('
            CREATE TABLE `item_toppings` (
                `id` bigint(20) UNSIGNED NOT NULL,
                `order_item_id` bigint(20) UNSIGNED NOT NULL,
                `topping_id` bigint(20) UNSIGNED NOT NULL
            );
        '));
        DB::statement(DB::raw('
            ALTER TABLE `item_toppings`
            ADD PRIMARY KEY (`id`),
            ADD KEY `item_toppings_order_item_id_foreign` (`order_item_id`),
            ADD KEY `item_toppings_topping_id_foreign` (`topping_id`);
        '));
        DB::statement(DB::raw('
            ALTER TABLE `item_toppings`
            ADD CONSTRAINT `item_toppings_order_item_id_foreign` FOREIGN KEY (`order_item_id`) REFERENCES `order_items` (`id`),
            ADD CONSTRAINT `item_toppings_topping_id_foreign` FOREIGN KEY (`topping_id`) REFERENCES `toppings` (`id`);
        '));

        DB::statement(DB::raw('
            ALTER TABLE `item_toppings`
            MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;            
        '));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('item_toppings');
        DB::statement(DB::raw('
            DROP TABLE `item_toppings`;
        '));
    }
}
