<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromoItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('promo_items', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->bigInteger('topping_id')->unsigned();
        //     $table->bigInteger('promo_id')->unsigned();
        //     $table->float('discount');

        //     $table->foreign('topping_id')->references('id')->on('toppings');
        //     $table->foreign('promo_id')->references('id')->on('promos');

        // });
        DB::statement(DB::raw('
            CREATE TABLE `promo_items` (
                `id` bigint(20) UNSIGNED NOT NULL,
                `topping_id` bigint(20) UNSIGNED NOT NULL,
                `promo_id` bigint(20) UNSIGNED NOT NULL,
                `discount` double(8,2) NOT NULL
            );
        '));
        DB::statement(DB::raw('
            ALTER TABLE `promo_items`
                ADD PRIMARY KEY (`id`),
                ADD KEY `promo_items_topping_id_foreign` (`topping_id`),
                ADD KEY `promo_items_promo_id_foreign` (`promo_id`);
        '));
        DB::statement(DB::raw('
            ALTER TABLE `promo_items`
            MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
        '));
        DB::statement(DB::raw('
            ALTER TABLE `promo_items`
            ADD CONSTRAINT `promo_items_promo_id_foreign` FOREIGN KEY (`promo_id`) REFERENCES `promos` (`id`),
            ADD CONSTRAINT `promo_items_topping_id_foreign` FOREIGN KEY (`topping_id`) REFERENCES `toppings` (`id`);
        '));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('promo_items');
        DB::statement(DB::raw('DROP TABLE promo_items'));
    }
}
