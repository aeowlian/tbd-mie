<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProcedureReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::unprepared(DB::raw('
        DROP PROCEDURE IF EXISTS `getBenefitReport`;
        CREATE PROCEDURE getBenefitReport(IN `time1` datetime, IN `time2` datetime)
        BEGIN
            create temporary table tempPrice
            select *,(order_price-discount-(realPriceMenu+sumRealPriceTopping)) as benefit from
            (
                select OrderId,idOrderItem,realPriceMenu,SUM(toppings.real_price) as sumRealPriceTopping,order_price,discount from 
                (
                    select OrderId,idOrderItem,realPriceMenu,topping_id,order_price,discount from
                    (
                        select OrderId,idOrderItem,menus.real_price as RealPriceMenu,order_price,discount from
                        (
                            select t1.id as OrderId,order_items.id as idOrderItem,menu_id,order_price,discount from 
                            (
                                select id 
                                from orders
                                where created_at >=`time1` AND created_at<=`time2`
                            )as t1 join order_items on t1.id=order_items.order_id
                        ) as t2 join menus on menus.id=t2.menu_id
                    ) as t3 join item_toppings on item_toppings.order_item_id=t3.idOrderItem
                )as t4 join toppings on t4.topping_id=toppings.id
                group by idOrderItem
                ) as t5;
                
        
            select * from tempPrice;
        
            drop temporary table tempPrice;
        
        END;
        '));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
