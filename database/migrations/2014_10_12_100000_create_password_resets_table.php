<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePasswordResetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement(DB::raw("CREATE TABLE `verify_users`(
            `email` varchar(255) NOT NULL,
            `token` varchar(255) NOT NULL
        )"));
        DB::statement(DB::raw("            
            ALTER TABLE `verify_users`
            ADD UNIQUE KEY `verify_users_email_unique` (`email`);"
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement(DB::raw('DROP TABLE verify_users'));
    }
}
