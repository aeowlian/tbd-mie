<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class AddProcedureForCustomerOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $current_date_time = Carbon::now()->toDateTimeString();
        DB::unprepared(DB::raw("
        DROP PROCEDURE IF EXISTS `insertOrder`;
        CREATE PROCEDURE insertOrder()
        BEGIN
            INSERT INTO
                `orders` (`created_at`)
            VALUES
                ('$current_date_time');

            SELECT max(id) as id FROM `orders`;
        END;           
        "));
        
        DB::unprepared(DB::raw('
        DROP PROCEDURE IF EXISTS `insertOrderItem`;
        CREATE PROCEDURE insertOrderItem(IN `order_id_input` INT, IN `menu_id_input` INT)
        BEGIN
            INSERT INTO
                `order_items` (`order_id`, `menu_id`,`order_price`)
            VALUES
                (`order_id_input`, `menu_id_input` , 0);
            
            SELECT max(id) as id FROM `order_items`;
        END;           
        '));

        DB::unprepared(DB::raw('
        DROP PROCEDURE IF EXISTS `insertItemToppings`;
        CREATE PROCEDURE insertItemToppings(IN `order_item_id_input` INT, IN `topping_id_input` INT)
        BEGIN
            INSERT INTO
                `item_toppings` (`order_item_id`, `topping_id`)
            VALUES
                (`order_item_id_input`, `topping_id_input`);

            SELECT max(id) as id FROM `item_toppings`;
        END;           
        '));

        DB::unprepared(DB::raw('
        DROP PROCEDURE IF EXISTS `updateOrderItemPrice`;
        CREATE PROCEDURE updateOrderItemPrice(IN `idOrderItem` integer)
        BEGIN
            DECLARE idMenu integer;
            DECLARE totalPrice integer;
            
            CREATE TEMPORARY TABLE IF NOT EXISTS  priceTable2(
                price integer
            );

            SELECT `menu_id` into idMenu FROM `order_items`
            WHERE `id`=`idOrderItem`;

            INSERT INTO priceTable2(price)
            SELECT `price` from `item_toppings` JOIN `toppings` ON toppings.id = item_toppings.topping_id
            WHERE `order_item_id` = `idOrderItem`;

            INSERT INTO priceTable2(price)
            SELECT `price` from `menus`
            WHERE `id`= idMenu;

            SELECT sum(price) into totalPrice from priceTable2;

            UPDATE
                `order_items`
            SET
                `order_price`= totalPrice
            WHERE
                `id`=idOrderItem;
            DROP temporary table priceTable2;
        END;
        '));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared(DB::raw('DROP PROCEDURE IF EXISTS `insertOrderItem`;'));
        DB::unprepared(DB::raw('DROP PROCEDURE IF EXISTS `insertItemToppings`;'));
        DB::unprepared(DB::raw('DROP PROCEDURE IF EXISTS `insertOrder`;'));
        DB::unprepared(DB::raw('DROP PROCEDURE IF EXISTS `updateOrderItemPrice`;'));
    }
}
