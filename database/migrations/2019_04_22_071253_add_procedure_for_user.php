<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProcedureForUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared(DB::raw(
            'DROP PROCEDURE IF EXISTS `register`;
            CREATE PROCEDURE `register` (IN `name` varchar(255),IN `emailin` varchar(255),IN `password` varchar(255),IN `phone` varchar(50),IN `address` varchar(255),IN `is_admin` tinyint(1))
            BEGIN
            INSERT INTO `users` (`name`,`email`,`password`,`phone`,`address`,`is_admin`) VALUES (`name`,`emailin`,`password`,`phone`,`address`,`is_admin`);
            SELECT * FROM `users` WHERE `email`=`emailin` LIMIT 1;
            END;          
        '));

        DB::unprepared(DB::raw("
            DROP PROCEDURE IF EXISTS `updateUser`;
            CREATE PROCEDURE `updateUser` (IN `idUser` INT ,IN `namein` varchar(255),IN `phonein` varchar(50),IN `addressin` varchar(255),IN `is_adminin` tinyint(1))
            BEGIN
                UPDATE 
                    `users` 
                SET
                    `name`=`namein`,
                    `phone`=`phonein`,
                    `address`=`addressin`,
                    `is_admin`=`is_adminin`
                WHERE
                    `id`=`idUser`;
            END;          
        "));

        DB::unprepared(DB::raw("
            DROP PROCEDURE IF EXISTS `checkToken`;
            CREATE PROCEDURE `checkToken` (IN `username` varchar(255),IN `tokenin` varchar(255) )
            BEGIN
                SELECT * FROM `verify_users` WHERE `email`=`username` AND `token`=`tokenin` LIMIT 1;
            END;          
        "));

        DB::unprepared(DB::raw("
            DROP PROCEDURE IF EXISTS `updateEmail`;
            CREATE PROCEDURE `updateEmail` (IN `username` varchar(255),IN `emailin` varchar(255) )
            BEGIN
                UPDATE
                    `users`
                SET
                    `email`=`emailin`
                WHERE
                    `email`=`username`;
            END
        "));

        DB::unprepared(DB::raw("
            DROP PROCEDURE IF EXISTS `changePassword`;
            CREATE PROCEDURE `changePassword` (IN `username` varchar(255),IN `passwordin` varchar(255) )
            BEGIN
                UPDATE 
                    `users`
                SET
                    `password`=`passwordin`
                WHERE
                    `email`=`username`;
            END;
        "));

        DB::unprepared(DB::raw("
            DROP PROCEDURE IF EXISTS `login`;
            CREATE PROCEDURE `login` (IN `username` varchar(255))
            BEGIN
            SELECT * FROM `users` WHERE `email`=`username`LIMIT 1;
            END;          
        "));

        DB::unprepared(DB::raw("
            DROP PROCEDURE IF EXISTS `sendToken`;
            CREATE PROCEDURE `sendToken` (IN `username` varchar(255),IN `token` varchar(255) )
            BEGIN
                INSERT INTO `verify_users` VALUES (`username`, `token`);
            END;   
        "));

        DB::unprepared(DB::raw("
            DROP PROCEDURE IF EXISTS `revokeToken`;
            CREATE PROCEDURE `revokeToken` (IN `username` varchar(255),IN `tokenin` varchar(255))
            BEGIN
                DELETE FROM `verify_users` WHERE `email`=`username` and `token`=`tokenin`;
            END;   
        "));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared(DB::raw("
            DROP PROCEDURE IF EXISTS `register`;
            DROP PROCEDURE IF EXISTS `updateUser`;
            DROP PROCEDURE IF EXISTS `checkToken`;
            DROP PROCEDURE IF EXISTS `updateEmail`;
            DROP PROCEDURE IF EXISTS `changePassword`;
            DROP PROCEDURE IF EXISTS `login`;
            DROP PROCEDURE IF EXISTS `sendToken`;
            DROP PROCEDURE IF EXISTS `revokeToken`;
        "));
        //
    }
}
