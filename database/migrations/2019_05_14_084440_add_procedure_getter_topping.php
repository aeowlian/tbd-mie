<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProcedureGetterTopping extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `indexTopping`;
            CREATE PROCEDURE indexTopping()
            BEGIN
                SELECT
                    *
                FROM
                    `toppings`;
            END; 
        '));

        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `getTopping`;
            CREATE PROCEDURE getTopping(IN `topping_id` bigint(20))
            BEGIN
                SELECT
                    *
                FROM
                    `toppings`
                WHERE
                    `id` = topping_id;
            END; 
        '));

        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `deleteTopping`;
            CREATE PROCEDURE deleteTopping(IN `topping_id` bigint(20))
            BEGIN
                DELETE FROM
                    `toppings`
                WHERE
                    `id` = topping_id;
            END; 
        '));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `indexTopping`;
        '));
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `getTopping`;
        '));
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `deleteTopping`;
        '));
    }
}
