<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProcedurePromo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `indexPromo`;
            CREATE PROCEDURE indexPromo()
            BEGIN
                SELECT
                    *
                FROM
                    `promos`;
            END; 
        '));

        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `getPromo`;
            CREATE PROCEDURE getPromo(IN `promo_id` bigint(20))
            BEGIN
                SELECT
                    *
                FROM
                    `promos`
                WHERE
                    `id` = promo_id;
            END; 
        '));

        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `deletePromo`;
            CREATE PROCEDURE deletePromo(IN `promo_id` bigint(20))
            BEGIN
                DELETE FROM
                    `promos`
                WHERE
                    `id` = promo_id;
            END; 
        '));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `indexPromo`;
        '));
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `getPromo`;
        '));
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `deletePromo`;
        '));
    }
}
