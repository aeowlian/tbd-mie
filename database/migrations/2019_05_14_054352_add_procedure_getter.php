<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProcedureGetter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `indexMenu`;
            CREATE PROCEDURE indexMenu()
            BEGIN
                SELECT
                    *
                FROM
                    `menus`;
            END; 
        '));

        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `getMenu`;
            CREATE PROCEDURE getMenu(IN `menu_id` bigint(20))
            BEGIN
                SELECT
                    *
                FROM
                    `menus`
                WHERE
                    `id` = menu_id;
            END; 
        '));

        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `deleteMenu`;
            CREATE PROCEDURE deleteMenu(IN `menu_id` bigint(20))
            BEGIN
                DELETE FROM
                    `menus`
                WHERE
                    `id` = menu_id;
            END; 
        '));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `indexMenu`;
        '));
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `getMenu`;
        '));
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `deleteMenu`;
        '));
    }
}
