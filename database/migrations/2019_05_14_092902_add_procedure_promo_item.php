<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProcedurePromoItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `indexPromoItem`;
            CREATE PROCEDURE indexPromoItem()
            BEGIN
                SELECT
                    *
                FROM
                    `promo_items`;
            END; 
        '));

        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `getPromoItem`;
            CREATE PROCEDURE getPromoItem(IN `promo_item_id` bigint(20))
            BEGIN
                SELECT
                    *
                FROM
                    `promo_items`
                WHERE
                    `id` = promo_item_id;
            END; 
        '));

        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `getPromoItemByPromo`;
            CREATE PROCEDURE getPromoItemByPromo(IN `promo_id` bigint(20))
            BEGIN
                SELECT
                    *
                FROM
                    `promo_items`
                WHERE
                    `promo_id` = promo_id;
            END; 
        '));

        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `deletePromoItem`;
            CREATE PROCEDURE deletePromoItem(IN `promo_item_id` bigint(20))
            BEGIN
                DELETE FROM
                    `promo_items`
                WHERE
                    `id` = promo_item_id;
            END; 
        '));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `indexPromoItem`;
        '));
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `getPromoItem`;
        '));
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `deletePromoItem`;
        '));
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `getPromoItemByPromo`;
        '));
    }
}
