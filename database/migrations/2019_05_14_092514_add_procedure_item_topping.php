<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProcedureItemTopping extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `indexItemTopping`;
            CREATE PROCEDURE indexItemTopping()
            BEGIN
                SELECT
                    *
                FROM
                    `item_toppings`;
            END; 
        '));

        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `getItemTopping`;
            CREATE PROCEDURE getItemTopping(IN `item_topping_id` bigint(20))
            BEGIN
                SELECT
                    *
                FROM
                    `item_toppings`
                WHERE
                    `id` = item_topping_id;
            END; 
        '));

        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `getItemToppingByOrderItem`;
            CREATE PROCEDURE getItemToppingByOrderItem(IN `order_item_id_input` bigint(20))
            BEGIN
                SELECT
                    *
                FROM
                    `item_toppings` JOIN `toppings` ON item_toppings.topping_id = toppings.id
                WHERE
                    `order_item_id` = order_item_id_input;
            END; 
        '));

        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `deleteItemTopping`;
            CREATE PROCEDURE deleteItemTopping(IN `item_topping_id` bigint(20))
            BEGIN
                DELETE FROM
                    `item_toppings`
                WHERE
                    `id` = item_topping_id;
            END; 
        '));

        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `deleteItemToppingByOrderItem`;
            CREATE PROCEDURE deleteItemToppingByOrderItem(IN `order_item_id_in` bigint(20))
            BEGIN
                DELETE FROM
                    `item_toppings`
                WHERE
                    `order_item_id` = order_item_id_in;
            END; 
        '));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `indexItemTopping`;
        '));
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `getItemTopping`;
        '));
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `getItemToppingByOrderItem`;
        '));
        DB::unprepared(DB::raw('
            DROP PROCEDURE IF EXISTS `deleteItemTopping`;
        '));
    }
}
