<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProcedureGenerateRecomendation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared(DB::raw("
        DROP PROCEDURE IF EXISTS `searchRecomendation`;
        CREATE PROCEDURE searchRecomendation(IN `idOrderItem` bigint(20))
        BEGIN
            declare str varchar(255);
            declare finished INTEGER DEFAULT 0;
            declare tempToppingId,isFirst,countTopping integer;
            
            declare orderTopping cursor for
            select topping_id 
            from item_toppings 
            where order_item_id=idOrderItem;
            DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = 1;
            
            create temporary table tempToppings(id int,order_item_id int,topping_id int);
            create temporary table tempToppings2(id int,order_item_id int,topping_id int);
            insert into tempToppings2 select * from item_toppings;
            
            set isFirst=1;
            set @str='select toppings.id as id, name, toppings.price as price, toppings.path as path, picked from (select topping_id as recomendation,count(topping_id) as picked from tempToppings where ';
            open orderTopping;
            topping_loop: LOOP
                FETCH OrderTopping INTO tempToppingId;
                IF finished = 1 THEN LEAVE topping_loop;
                END IF;
                
                IF isFirst=0 THEN
                    set @str=concat(@str,' AND ');
                END IF;
                set @str=concat(@str,'topping_id !=',tempToppingId);
                truncate tempToppings;
                insert into tempToppings
                select id,item_toppings.order_item_id,topping_id from (
                    select distinct order_item_id from tempToppings2
                    where topping_id=tempToppingId
                    ) as t1  left join item_toppings on t1.order_item_id=item_toppings.order_item_id;
                    
                truncate tempToppings2;
                insert into tempToppings2
                select * from tempToppings;
                
                IF isFirst=1 THEN
                    set isFirst=0;
                END IF;
            end loop;
            set @str=concat(@str,' group by topping_id order by picked desc limit 3)as t1 join toppings on recomendation=toppings.id');
        
            select count(*)
            into countTopping
            from item_toppings 
            where order_item_id=idOrderItem;

            if countTopping > 0 THEN
                PREPARE stmt FROM @str;
                EXECUTE stmt;
                DEALLOCATE PREPARE stmt;
            ELSE
                select 'null';
            END IF;
            
            CLOSE OrderTopping;
            drop temporary table tempToppings;
            drop temporary table tempToppings2;
            
            END;
        "));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
