<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToppingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('toppings', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->string('name', 50);
        //     $table->integer('price');
        //     $table->string('path', 255);
        // });
        DB::statement(DB::raw('
            CREATE TABLE `toppings` (
                `id` bigint(20) UNSIGNED NOT NULL,
                `name` varchar(50),
                `real_price` int(11) NOT NULL,
                `price` int(11) NOT NULL,
                `path` varchar(255) NOT NULL
            );
        '));

        DB::statement(DB::raw('
            ALTER TABLE `toppings`
            ADD PRIMARY KEY (`id`);
        '));

        DB::statement(DB::raw('
            ALTER TABLE `toppings`
            MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
        '));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('toppings');
        DB::statement(DB::raw('
            DROP TABLE `toppings`;
        '));
    }
}
