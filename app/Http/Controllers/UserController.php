<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Hasher;
use Illuminate\Support\Facades\DB;
use Illuminate\Notifications\Notifiable;
use App\Notifications\Token;
use App\Http\Resources\DefaultResource;
use Validator;
class UserController extends Controller
{
    use Notifiable;
    private $email;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $status = !$request->validate([
            "name"=>"required",
            "email"=>"required",
            "password"=>"required|confirmed",
            "phone"=>"required",
            "address"=>"required",
        ]);
        if($status){
            return response()->json(['message'=>'invalid data!'],400);
        }
        $data = $request->only(["name","email","password","phone","address"]);
        $password = Hash::make($data['password']);
        $name = $data['name'];
        $email = $data['email'];
        $phone = $data['phone'];
        $address = $data['address'];
        $user = DB::select(DB::raw("call register('$name','$email','$password','$phone','$address',0)"));
        if(is_array($user)){
            $user = $user[0];
        }
        $hasil = [];
        $hasil['token'] = Hasher::encrypt($user->id);
        return new DefaultResource($hasil);
    }

    public function login(Request $request){
        $status = !$request->validate([
            "email" => "required",
            "password" => "required"
            ]);
        if($status){
            return response()->json(['message'=>'invalid data!'],400);
        }
        $email = $request->email;
        $user = DB::select(DB::raw("call login('$email')"));
        if(is_array($user) && !empty($user)){
            $user = $user[0];
        }
        if(!$user || empty($user)){
            return response()->json(['message'=> "email not exist!"],400);
        }
        if(!Hash::check($request->password,$user->password)){
            return response()->json(['message'=>'wrong password!'],400);
        }
        $hasil = [];
        $hasil['token'] = Hasher::encrypt($user->id);
        return new DefaultResource($hasil);
    }

    public function see(Request $request){
        $status = !$request->validate([
            'id' => "required"
        ]);
        if($status){
            return response()->json(['message'=>'invalid id!'],400);
        }
        $id = Hasher::decrypt($request->id);
        $user = DB::select(DB::raw("SELECT `name`,`email`,`phone`,`address`,`is_admin` FROM `users` WHERE `id`=$id LIMIT 1"));
        return new DefaultResource($user);
    }

    public function isAdmin(Request $request){
        $status = !$request->validate([
            'id' => "required"
        ]);
        if($status){
            return response()->json(['message'=>'invalid id!'],400);
        }
        $id = Hasher::decrypt($request->id);
        $user = DB::select(DB::raw("SELECT `is_admin` FROM `users` WHERE `id`=$id LIMIT 1"));
        return new DefaultResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $status = !$request->validate([
            'id' => "required",
            'email' => "required|email",
            'name' => "required",
            'phone' => "required",
            'address' => "required",
            'is_admin' => "required"
        ]);
        
        if($status){
            return response()->json(['message'=>'invalid data!'],400);
        }
        $id = Hasher::decrypt($request->id);
        $email = $request->email;
        $name = $request->name;
        $phone = $request->phone;
        $address = $request->address;
        $is_admin = $request->is_admin;
        if(isset($request['password'])){
            $this->changePassword($request);
        }
        DB::select(DB::raw("call updateUser($id,'$name','$phone','$address',$is_admin)"));
        return response()->json(['message' => "success to update user!"],200);
    }

    public function changePassword(Request $request){
        $status = !$request->validate([
            'password' => "confirmed",
            'email' => 'required|email',
            'token' => 'required'
        ]);
        
        if($status){
            return response()->json(['message'=>'invalid data!'],400);
        }
        $password = Hash::make($request->password);
        $email = $request->email;
        $tokens = DB::select(DB::raw("call checkToken('$email','$request->token')"));
        if(empty($tokens)){
            return response()->json(['message'=>'invalid Token!'],400);
        }
        DB::select(DB::raw("call changePassword('$email','$password')"));
        DB::select(DB::raw("call revokeToken('$email','$request->token')"));
        return response()->json(['message'=>"success to update password!"],200);
    }

    public function changeEmail(Request $request){
        $status = !$request->validate([
            'email' => 'required|email',
            'email2' => 'required|email',
            'token' => 'required'
        ]);
        if($status){
            return response()->json(['message'=>'invalid data!'],400);
        }
        $email = $request->email;
        $email2 = $request->email2;
        $tokens = DB::select(DB::raw("call checkToken('$email','$request->token')"));
        if(empty($tokens)){
            return response()->json(['message'=>'invalid Token!'],400);
        }
        DB::select(DB::raw("call updateEmail('$email','$email2')"));
        DB::select(DB::raw("call revokeToken('$email','$request->token')"));
        return response()->json(['message'=>"success to update email!"],200);
    }

    public function sendToken(Request $request){
        $status = !$request->validate([
            'email' => 'required|email',
            'url' => 'required',
            'type' => 'required'
        ]);
        if($status){
            return response()->json(['message'=>'invalid data!'],400);
        }
        $email = $request->email;
        $token = Hasher::generateToken();
        try{
            DB::statement(DB::raw("
                call sendToken('$email','$token')
            "));
            $this->email =$email;
            $request->token=$token;
            $this->notify(new Token($request,$request->url,$request->type));
        }catch(\Exception $e){
            
        }
        return response()->json(['message' => 'email has been sent!'],200);
    }

    public function checkToken(Request $request){
        $status = !$request->validate([
            'token' => 'validate',
            'email' => 'required|email'
        ]);
        if($status){
            return response()->json(['message'=>'invalid data!'],400);
        }
        $token = $request->token;
        $email = $request->email;
        $hasil = DB::unprepared(DB::raw("call checkToken('$email','$token')"));
        if($hasil){
            return response()->json(['message'=>'token valid!'],200);
        }else{
            return response()->json(['message'=>'token invalid!'],422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
