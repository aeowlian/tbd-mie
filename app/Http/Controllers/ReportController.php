<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\DefaultResource;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showReport(Request $request)
    {
        // $menu = DB::select(DB::raw("call getMenu($id)"));
        // return new DefaultResource($menu);
        $time1=$request->time1;
        $time2=$request->time2;

        $report=DB::select(DB::raw("call getBenefitReport('$time1','$time2')"));
        return new DefaultResource($report);
    }

    public function showCombinationReport(Request $request){
        //
        $n=$request->number;
        $report2=DB::select(DB::raw("call mostPopular($n)"));
        return new DefaultResource($report2);
    }

    public function showCombinationReportAPI(Request $request){
        $n=$request->number;
        $report2=DB::select(DB::raw("call mostPopular($n)"));
        if(empty($report2)){
            return [];
        }
        return DB::select(DB::raw("SELECT `name`,`price`,`real_price` FROM toppings WHERE id IN (".$report2[0]->combination.")"));
    }

}
