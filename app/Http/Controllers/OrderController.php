<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\DefaultResource;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = DB::select(DB::raw('call indexOrder()'));
        return new DefaultResource($orders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = DB::select(DB::raw('call insertOrder()'));

        return new DefaultResource($response, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = DB::select(DB::raw("call getOrder($id)"));
        $order['order_items'] = DB::select(DB::raw("call getOrderItemByOrder($id)"));
        return new DefaultResource($order);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $response = "Can't update order.";
        return new DefaultResource($response, 203);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = "Can't manually delete order.";
        return new DefaultResource($order, 204);
    }

    public function cleanOrder(){
        //TODO clean order, order item, item topping
    }

    public function confirmOrder($id) {
        $flag = true;
        $orderItems = DB::select(DB::raw("call getOrderItemByOrder($id)"));
        
        foreach ($orderItems as $orderItem) {
            if($orderItem->status == 0) $flag = false;
        }

        if($flag){
            $discount = DB::select(DB::raw("call calculateDiscount($id)"));
            // return $discount;
            $orderItemId = $discount[0]->OrderItemID;
            $discountPrice = $discount[0]->discountPrice;
            if($discountPrice == null) $discountPrice = 0;
            DB::statement(DB::raw("call updateOrderItemDiscount($orderItemId, $discountPrice)"));

            $order = DB::select(DB::raw("call updateOrder($id, 1)"));
            // return $order;
            $status = $order[0]->status;
            // return $status;
            if($status == 1){
                $orderItems = DB::select(DB::raw("call getOrderItemByOrder($id)"));
        
                foreach ($orderItems as $orderItem) {
                    $menu = DB::select(DB::raw("call getMenu($orderItem->menu_id)"));
                    $orderItem->menu = $menu[0];

                    $toppings = DB::select(DB::raw("call getItemToppingByOrderItem($orderItem->id)"));
                    $orderItem->toppings = $toppings;
                }
                $order[0]->orderItems = $orderItems;
            }

            return new DefaultResource($order);
        }else{
            $order = new DefaultResource(DB::select(DB::raw("call getOrder($id)")));
            return $order->response()->setStatusCode(200);
        }
    }
}
