<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\DefaultResource;
use Illuminate\Support\Facades\DB;

class ItemToppingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $toppings = DB::select(DB::raw('call indexItemTopping()'));
        return new DefaultResource($toppings);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $orderItemId = $request->orderItemId;
        $toppingId = $request->toppingId;

        $response = DB::select(DB::raw("call insertItemToppings($orderItemId, $toppingId)"));

        return new DefaultResource($response, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $topping = DB::select(DB::raw("call getItemTopping($id)"));
        return new DefaultResource($topping);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->name;
        $price = $request->price;
        $path = $request->path;

        DB::select(DB::raw("call updateItemTopping($id, '$name', $price, '$path')"));

        $response = [
            'name' => $name,
            'price' => $price,
            'path' => $path
        ];
        return new DefaultResource($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $topping = DB::select(DB::raw("call getItemTopping($id)"));
        DB::select(DB::raw("call deleteItemTopping($id)"));
        return new DefaultResource($topping, 204);
    }
}
