<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\DefaultResource;
use Illuminate\Support\Facades\DB;

class OrderItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orderItems = DB::select(DB::raw('call indexOrderItem()'));
        return new DefaultResource($orderItems);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order_id = $request->order_id;
        $menu_id = $request->menu_id;
        $toppings = $request->toppings;

        $order_item_id = DB::select(DB::raw("call insertOrderItem($order_id, $menu_id)"));
        $order_item_id = $order_item_id[0]->id;

        if($toppings != null){
            foreach ($toppings as $topping) {
                $toppingId = $topping['id'];
                DB::select(DB::raw("call insertItemToppings($order_item_id, $toppingId)"));
            }
        }

        $recomendation = DB::select(DB::raw("call searchRecomendation($order_item_id)"));

        $response = [
            'recommendation' => $recomendation,
            'order_item_id' => $order_item_id
        ];

        return new DefaultResource($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $orderItem = DB::select(DB::raw("call getOrderItem($id)"));
        return new DefaultResource($orderItem);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $response = "Can't update orderItem.";
        return new DefaultResource($response, 203);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = DB::select(DB::raw("call getOrderItem($id)"));

        $toppings = DB::select(DB::raw("call deleteItemToppingByOrderItem($id)"));

        DB::select(DB::raw("call deleteOrderItem($id)"));
        return new DefaultResource($response, 204);
    }

    public function confirmOrderItem($id) {
        $orderItem = DB::select(DB::raw("call updateOrderItem($id, 1)"));
        DB::statement(DB::raw("call updateOrderItemPrice($id)"));
        return new DefaultResource($orderItem);
    }
}
