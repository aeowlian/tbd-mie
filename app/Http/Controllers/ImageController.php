<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FileUpload;

class ImageController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = '';
            if($request->get('image')){
            $image = $request->get('image');
            $name = $request->get('name');
            \Image::make($image)->save(public_path('images/').$name.'.png');
        }
    
        $image= new FileUpload();
        $image->image_name = $name;
        $image->save();
    
        return response()->json(['success' => 'You have successfully uploaded an image'], 200);
    }
    
}
