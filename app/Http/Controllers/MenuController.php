<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\DefaultResource;
use Illuminate\Support\Facades\DB;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = DB::select(DB::raw('call indexMenu()'));
        return new DefaultResource($menus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = $request->name;
        $price = $request->price;
        $real_price = $request->real_price;
        $path = '/images/' . $name . '.jpg';

        DB::statement(DB::raw("call createMenu('$name',$real_price, $price, '$path')"));

        $response = [
            'name' => $name,
            'price' => $price,
            'real_price' => $real_price,
            'path' => $path
        ];
        return new DefaultResource($response, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $menu = DB::select(DB::raw("call getMenu($id)"));
        return new DefaultResource($menu);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->name;
        $price = $request->price;
        $path = $request->path;

        DB::select(DB::raw("call updateMenu($id, '$name',0, $price, '$path')"));

        $response = [
            'name' => $name,
            'price' => $price,
            'path' => $path
        ];
        return new DefaultResource($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu = DB::select(DB::raw("call getMenu($id)"));
        DB::select(DB::raw("call deleteMenu($id)"));
        return new DefaultResource($menu, 204);
    }
}
