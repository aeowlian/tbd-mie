<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use App\User;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class Token extends Notification
{
    use Queueable;

    private $user;
    private $url;
    private $type;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $url = null,$type = "email")
    {
      $this->url = $url;
      $this->user = $user;
      $this->type = $type;
      $this->initUrl();
    }

    private function initUrl(){
      $data = [
        'email' => $this->user->email,
        'token' => $this->user->token,
      ];

      $this->url .= "?email=".$data['email'].'&token='.$data['token']."&type=".($this->type);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('TBD MIE Action')
                    ->line('Please Click this link')
                    ->action('action link', $this->url)
                    ->line('Thank you!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
