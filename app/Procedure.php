<?php

namespace App;

class Procedure
{
    public static function callProcedure($name, $parameter = null){
        if($parameter != null){
            DB::unprepared(DB::raw("call $name($parameter)"));
        }else{
            DB::unprepared(DB::raw("call $name()"));
        }
    }
}
