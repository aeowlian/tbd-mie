<?php

namespace App;

class Hasher
{
    private const KEY = "jQFGXxMokTR4VUdVTjBBiqmRAnWr94TDNBl3w8NvRuE=";
    public static function encrypt($data) {
        $encryption_key = base64_decode(SELF::KEY);
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
        $encrypted = openssl_encrypt($data, 'aes-256-cbc', $encryption_key, 0, $iv);
        return base64_encode($encrypted . '::' . $iv);
    }
     
    public static function decrypt($data) {
        $encryption_key = base64_decode(SELF::KEY);
        list($encrypted_data, $iv) = explode('::', base64_decode($data), 2);
        return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);
    }

    public static function generateToken(){
        return str_random(255);
    }
}
