<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::prefix('v1')->group(function () {
    Route::resources([
        'menu' => 'MenuController',
        'topping' => 'ToppingController',
        'order' => 'OrderController',
        'order-item' => 'OrderItemController',
        'item-topping' => 'ItemToppingController'
    ]);
    Route::group(['prefix' => 'report'], function(){
        Route::post("/popular-api", "ReportController@showCombinationReportAPI");
        Route::post("/benefit", "ReportController@showReport");
        Route::post("/popular", "ReportController@showCombinationReport");
    });
    Route::group(['prefix' => 'user'],function(){
        Route::post("/register","UserController@register");
        Route::post("/login","UserController@login");
        Route::post("/detail","UserController@see");
        Route::post("/is-admin","UserController@isAdmin");
        Route::post("/update","UserController@update");
        Route::post("/change-password","UserController@changePassword");
        Route::post("/change-email","UserController@changeEmail");
        Route::post("/send","UserController@sendToken");
        Route::get("/check","UserController@checkToken");
    });
    Route::group(['prefix' => 'confirmation'], function() {
        Route::get("/order-item/{id}", 'OrderItemController@confirmOrderItem');
        Route::get("/order/{id}", 'OrderController@confirmOrder');
    });
});